from statistics import mode
from tabnanny import verbose
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.
class Paciente(models.Model):
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=150)
    dni = models.IntegerField(validators=[MinValueValidator(5000), MaxValueValidator(99999999)])
    nacimiento = models.DateField()
    telefono = models.CharField(max_length=50, null=True, blank=True)
    obra_social = models.CharField(max_length=150)
    localidad = models.CharField(max_length=150, null=True, blank=True)
    direccion = models.CharField(max_length=150)

    class Meta:
        verbose_name = "Paciente"
        verbose_name_plural = "Pacientes"

    def __str__(self):
        return self.nombre + " - " + self.apellido