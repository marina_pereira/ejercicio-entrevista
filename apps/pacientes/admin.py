from django.contrib import admin
from django.contrib.admin import register

from apps.pacientes.models import Paciente

# Register your models here.
@register(Paciente)
class PacienteAdmin(admin.ModelAdmin):
    list_display = ["nombre", "apellido", "dni", "nacimiento", "telefono", "obra_social", "localidad", "direccion"]
    