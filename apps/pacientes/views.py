from django.shortcuts import render
from .serializers import PacienteSerializer
from rest_framework import viewsets
from .models import Paciente
# Create your views here.

class PacienteViewSet(viewsets.ModelViewSet):
    queryset = Paciente.objects.all()
    serializer_class = PacienteSerializer