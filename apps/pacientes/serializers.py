from rest_framework import serializers
from apps.pacientes.models import Paciente
from datetime import datetime
from dateutil.relativedelta import relativedelta


class PacienteSerializer(serializers.ModelSerializer):
    nombre_completo = serializers.SerializerMethodField('get_nombre_compĺeto')
    edad = serializers.SerializerMethodField('get_edad')

    def get_nombre_compĺeto(self, obj):
        if obj.nombre and obj.apellido:
            return obj.nombre + " " + obj.apellido
        else:
            return "No Ingreso Nombre/Apellido"

    def get_edad(self, obj):
        if obj.nacimiento:
            edad = relativedelta(datetime.now(), obj.nacimiento)
            return str(edad.years) + " años"
        else:
            return "No tiene fecha de nacimiento"
    
    class Meta:
        model = Paciente
        fields = ["nombre_completo", "edad", "nombre", "apellido", "dni", "nacimiento", "telefono", "obra_social", "localidad", "direccion"]